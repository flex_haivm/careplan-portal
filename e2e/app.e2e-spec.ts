import { FlexwebappPage } from './app.po';

describe('flexwebapp App', () => {
  let page: FlexwebappPage;

  beforeEach(() => {
    page = new FlexwebappPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
