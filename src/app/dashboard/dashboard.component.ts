import {Component, ViewEncapsulation, ViewChild} from '@angular/core';
import {Http, Response, Headers, HttpModule} from '@angular/http';
import * as moment from 'moment/moment.js';
import {SharedService} from '../shared/shared.service';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs/Observable';
import * as $ from 'jquery';
import {TranslateService} from '@ngx-translate/core';
import {Page} from '../shared/models/page';
import {PagedData} from '../shared/models/paged-data';
import {CareplanTemplate} from './models/careplan-template';
import {AssessmentTemplate} from './models/assessment-template';
import {TherapyTemplate} from './models/therapy-template';
import {forEach} from '@angular/router/src/utils/collection';
import {PrettyJsonComponent} from 'angular2-prettyjson';

@Component({
    entryComponents: [PrettyJsonComponent],
    templateUrl: 'dashboard.component.html',
    styleUrls: [
        '../../../node_modules/@swimlane/ngx-datatable/release/themes/material.css',
        '../../../node_modules/@swimlane/ngx-datatable/release/assets/icons.css'
    ],
    encapsulation: ViewEncapsulation.None,
    providers: [SharedService]
})
export class DashboardComponent {
    @ViewChild('dataTable') table: any;
    rows = new Array<CareplanTemplate>();
    dataToDisplay: any;
    isExpandAll = false;

    constructor(private http: Http, private translate: TranslateService) {
    }

    addData() {
        const newData = new CareplanTemplate();
        newData.name = '';
        newData.description = '';
        this.rows.push(newData);
    }

    removeData(rowIndex) {
        // console.log('remove', rowIndex);
        this.rows.splice(rowIndex, 1);
    }

    addRecord(row, target) {
        // console.log(row);
        if (target === 'assessment') {
            const newAssetment = new AssessmentTemplate();
            newAssetment.name = '';
            newAssetment.description = '';
            newAssetment.instructions = '';
            newAssetment.unitOfMeasure = '';
            this.rows[row.rowIndex].assessments.push(newAssetment);
        } else if (target === 'therapy') {
            const newTherapy = new TherapyTemplate();
            newTherapy.name = '';
            newTherapy.description = '';
            newTherapy.instructions = '';
            newTherapy.unitOfMeasure = '';
            this.rows[row.rowIndex].therapies.push(newTherapy);
        }
    }

    removeRecord(row, childRowIndex, target) {
        // console.log(row);
        // console.log(childRowIndex);
        if (target === 'assessment') {
            this.rows[row.rowIndex].assessments.splice(childRowIndex, 1);
        } else if (target === 'therapy') {
            this.rows[row.rowIndex].therapies.splice(childRowIndex, 1);
        }
    }

    updateValue(event, cell, rowIndex) {
        // console.log(event.target);
        if (event.target.value !== '') {
            $(event.target).next('span').css('display', 'none');
        } else {
            $(event.target).next('span').css('display', 'block');
        }
        // console.log('inline editing rowIndex', rowIndex);
        this.rows[rowIndex][cell] = event.target.value;
        // this.rows = [...this.rows];
        console.log('UPDATED!', this.rows[rowIndex][cell]);

        this.dataToDisplay = JSON.stringify(this.table.rows, null, 4);
    }

    updateChildValue(event, cell, parentRow, childRowIndex, target) {
        console.log(event.target.value);
        if (event.target.value !== '') {
            $(event.target).next('span').css('display', 'none');
        } else {
            $(event.target).next('span').css('display', 'block');
        }

        if (target === 'assessment') {
            // console.log('Parent row', parentRow);
            // console.log('Child row index', childRowIndex);
            this.rows[parentRow.rowIndex].assessments[childRowIndex][cell] = event.target.value;
            // this.rows = [...this.rows];
            // console.log('UPDATED!', this.rows[parentRow.rowIndex].assessments[childRowIndex][cell]);
        } else if (target === 'therapy') {
            // console.log('Parent row', parentRow);
            // console.log('Child row index', childRowIndex);
            this.rows[parentRow.rowIndex].therapies[childRowIndex][cell] = event.target.value;
            // this.rows = [...this.rows];
            // console.log('UPDATED!', this.rows[parentRow.rowIndex].therapies[childRowIndex][cell]);
        }

        this.dataToDisplay = JSON.stringify(this.table.rows, null, 4);
    }

    exportJson(rows) {
        // console.log(this.rows)
        let dataInfoBlank: boolean;
        let childAssetmentBlank: boolean;
        let childTherapyBlank: boolean;
        this.rows.forEach(function (e) {
            console.log(e);
            if (e.name !== '' && e.description !== '') {
                e.assessments.forEach(function (assessment) {
                    if (assessment.name !== ''
                        && assessment.description !== ''
                        && assessment.instructions !== ''
                        && assessment.unitOfMeasure !== '') {

                        // console.log('not blank');
                        childAssetmentBlank = false;
                    } else {
                        childAssetmentBlank = true;
                    }
                });
                if (!childAssetmentBlank) {
                    e.therapies.forEach(function (therapy) {
                        if (!(therapy.name !== ''
                                && therapy.description !== ''
                                && therapy.instructions !== ''
                                && therapy.unitOfMeasure !== '')) {

                            childTherapyBlank = true;
                        } else {
                            // console.log('not blank');
                            childTherapyBlank = false;
                        }
                    });
                }
                dataInfoBlank = false;
            } else {
                dataInfoBlank = true;
            }
        });
        console.log('dataInfoBlank ' + dataInfoBlank);
        console.log('childAssetmentBlank ' + childAssetmentBlank);
        console.log('childTherapyBlank ' + childTherapyBlank);

        // if (!dataInfoBlank && !childAssetmentBlank && !childTherapyBlank) {
        //     console.log(JSON.stringify(this.table.rows, null, 4));
        //     this.dataToDisplay = JSON.stringify(this.table.rows, null, 4);
        //     this.table.rowDetail.collapseAllRows(rows);
        // } else if (dataInfoBlank === undefined && childAssetmentBlank === undefined && childTherapyBlank === undefined) {
        //     console.log('No data to export');
        // } else {
        //     console.log('All fields are not filled');
        //     this.table.rowDetail.expandAllRows(rows);
        // }
    }

    toggleExpandRow(row, rowIndex) {
        row.rowIndex = rowIndex;
        console.log('Toggled Expand Row!', row);
        this.table.rowDetail.toggleExpandRow(row);
    }

    collapseAllRows(row) {
        this.table.rowDetail.collapseAllRows(row);
        this.isExpandAll = false;
    }

    expandAllRows(row) {
        this.table.rowDetail.expandAllRows(row);
        this.isExpandAll = true;
    }

    onDetailToggle(event) {
        console.log('Detail Toggled', event);
    }
}
