import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {PopoverModule} from 'ngx-bootstrap/popover';
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import {NgsRevealModule} from 'ng-scrollreveal';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';

import {DashboardComponent} from './dashboard.component';
import {DashboardRoutingModule} from './dashboard-routing.module';
import {MomentModule} from 'angular2-moment';
import {TabsModule} from 'ngx-bootstrap/tabs';
// import { FormsModule } from '@angular/forms';
// import { FormWizardModule } from 'angular2-wizard';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {PrettyJsonModule} from 'angular2-prettyjson';


@NgModule({
    imports: [
        CommonModule,
        DashboardRoutingModule,
        BsDropdownModule,
        NgsRevealModule,
        MomentModule,
        TabsModule,
        TooltipModule.forRoot(),
        PopoverModule.forRoot(),
        TranslateModule.forChild(),
        NgxDatatableModule,
        PrettyJsonModule
    ],
    declarations: [DashboardComponent]
})
export class DashboardModule {
}
