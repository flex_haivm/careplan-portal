export class AssessmentTemplate {
  name: string;
  description: string;
  type: string;
  instructions: string;
  unitOfMeasure: string;

  constructor(values: Object = {}) {
      Object.assign(this, values);
  }
}
