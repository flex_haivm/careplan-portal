import {AssessmentTemplate} from './assessment-template';
import {TherapyTemplate} from './therapy-template';

export class CareplanTemplate {
    rowIndex: number;
    name: string;
    description: string;
    assessments = new Array<AssessmentTemplate>();
    therapies = new Array<TherapyTemplate>();

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
