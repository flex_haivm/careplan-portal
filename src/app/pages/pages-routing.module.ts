import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { P403Component } from './403.component';
import { P404Component } from './404.component';
import { P500Component } from './500.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '403',
        component: P403Component,
        data: {
          title: 'Page 403'
        }
      }, {
        path: '404',
        component: P404Component,
        data: {
          title: 'Page 404'
        }
      },
      {
        path: '500',
        component: P500Component,
        data: {
          title: 'Page 500'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
