import {AbstractControl, ValidatorFn} from '@angular/forms';

export function flexPasswordValidator(required: boolean): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        if (required && (!control.value || control.value.trim() == '')) {
            return {'required': true, 'message': 'This field cannot be empty.'}
        } else if (control.value && control.value.trim() == '') {
            return null;
        }
        if (control.value.length < 8) {
            return {'length': true, 'message': 'This field must be at least 8 characters.'}
        }
        if (!/(.*[A-Z].*)$/.test(control.value)) {
            return {'uppercase': true, 'message': 'This field must contains at least an uppercase character.'}
        }
    };
}

export function flexEmailValidator(required: boolean): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        if (required && (!control.value || control.value.trim() == '')) {
            return {'required': true, 'message': 'This field cannot be empty.'}
        } else if (control.value && control.value.trim() == '') {
            return null;
        }
        if (/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(control.value)) {
            return {'format': true, 'message': 'This field has incorrect format.'}
        }
    };
}

export function flexUsernameValidator(required: boolean): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        if (required && (!control.value || control.value.trim() == '')) {
            return {'required': true, 'message': 'This field cannot be empty.'}
        } else if (control.value && control.value.trim() == '') {
            return null;
        }
        if (control.value.length < 4) {
            return {'length': true, 'message': 'This field must be at least 4 characters.'}
        }
    };
}
