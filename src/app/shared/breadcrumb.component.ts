import { Component, Output, Input } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { SharedService } from '../shared/shared.service';
import 'rxjs/add/operator/filter';

@Component({
  selector: 'app-breadcrumbs',
  template: `
      <ng-template ngFor let-breadcrumb [ngForOf]="breadcrumbs" let-last=last>
      <li class="breadcrumb-item" *ngIf="breadcrumb.label.title&&breadcrumb.url.substring(breadcrumb.url.length-1) == '/'||breadcrumb.label.title&&last" [ngClass]="{active: last}">
          <a *ngIf="!last" [routerLink]="breadcrumb.url">{{breadcrumb.label.title}}</a>
          <span *ngIf="last" [routerLink]="breadcrumb.url">{{breadcrumb.label.title}}</span>
      </li>
      <li class="breadcrumb-menu d-md-down-none" *ngIf="last">
          <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
              <ng-template ngFor let-menu [ngForOf]="breadcrumb_menu">
                  <a class="btn" [ngClass]="{'disabled': menu.isDisabled}" href="javascript:void(0)" (click)="menu.click()"><i class="{{menu.icon}}" [ngClass]="{'icon-refresh-animate': menu.isDisabled}"></i> {{menu.label}}</a>
              </ng-template>
          </div>
      </li>
    </ng-template>`,
  providers: [SharedService]
})
export class BreadcrumbsComponent {
  breadcrumbs: Array<Object>;
  breadcrumb_menu: Array<Object>;
  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) {
    SharedService.breadcrumbMenu.subscribe((data) => {
      this.breadcrumb_menu = data;
    });
    this.router.events.filter(event => event instanceof NavigationEnd).subscribe((event) => {
      this.breadcrumbs = [];
      let currentRoute = this.route.root,
        url = '';
      do {
        const childrenRoutes = currentRoute.children;
        currentRoute = null;
        childrenRoutes.forEach(route => {
          if (route.outlet === 'primary') {
            const routeSnapshot = route.snapshot;
            url += '/' + routeSnapshot.url.map(segment => segment.path).join('/');
            this.breadcrumbs.push({
              label: route.snapshot.data,
              url: url
            });
            currentRoute = route;
          }
        });
      } while (currentRoute);
    });

  }

  /**
   * name
   */
  public dost() {
    console.log('here');
  }
}
