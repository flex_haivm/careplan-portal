import { Injectable, Output, EventEmitter } from '@angular/core';
@Injectable()
export class SharedService {
    constructor() { }
    public static breadcrumbMenu = new EventEmitter<Array<Object>>();
}