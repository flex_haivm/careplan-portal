export const environment = {
  production: true,
  tableauUrl: "http://35.199.190.216",
  tokenUrl: "http://35.197.89.198/api/v1/token",
  accountUrl: "https://flexdigitalhealth-dev.apigee.net",
  configurationServiceUrl: "http://product-1.staging.flexdigitalhealth.com/configuration-service/api/v1/configurations"
};
