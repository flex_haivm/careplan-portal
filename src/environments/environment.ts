// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  // tableauUrl: "http://35.199.179.49",
  tableauUrl: "http://35.225.122.185",
  tokenUrl: "http://35.197.98.57/api/v1/token",
  accountUrl: "https://flexdigitalhealth-dev.apigee.net",
  configurationServiceUrl: "http://product-1.dev.flexdigitalhealth.com/configuration-service/api/v1/configurations"
};
