export const environment = {
  production: true,
  tableauUrl: "http://35.199.170.239",
  tokenUrl: "http://35.197.67.117/api/v1/token",
  accountUrl: "https://flexdigitalhealth-dev.apigee.net",
  configurationServiceUrl: "http://product-1.qa.flexdigitalhealth.com/configuration-service/api/v1/configurations"
};
